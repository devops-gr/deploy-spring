# Exemplo de pipeline para o Deploy:

* [ ] Spring Boot application to Cloud Foundry with GitLab CI/CD


* Fonte: [Deploy a Spring Boot application to Cloud Foundry with GitLab CI/CD](https://docs.gitlab.com/ee/ci/examples/doc/ci/examples/deploy_spring_boot_to_cloud_foundry/index.html).

* Fonte: [Deploy a Spring Boot Application](https://gitlab.com/gitlab-org/project-templates/spring)
---

# Conhecimento prévio sobre DevOps:


1. Agile Development: **Desenvolvimento Ágil**

   * Code > Build

    > *Equipe de desenvolvimento contruindo as aplicações*

2. Continuous Integration: **Os códigos são testados e validados constantemente.**

   * Code > Build > Integrate > Test

    > Conceito de CI: quando as equipes de desenvolve e integra as suas aplicações com regularidades
para verificar se o código desenvolvido não quebra outras aplicações se as features não conflitam e etc..


3. Continuous Delivery: **Deploy em producao é manual.**

  * Code > Build > Integrate > Test > Release

  > Entrega de uma versao de uma aplicacao em um repositorio.
    Ou seja, o armazenamento de uma pacote dentro de um repositorio de artefatos.
   

4. Continuous Deployment: **Deploy automatico em produção**

  * Code > Build > Integrate > Test > Release > Deploy

  > Alto grau de maturidade do time e da empresa no processo de desenvolvimento.
    

5. DevOps: **Cultura implementada na equipe com tudo automatizado**

 * Code > Build > Integrate > Test > Release > Deploy > Operate

    

# Conhecimento prévio para montar a Pipeline:


* [ ] Entender ou propor o fluxo de gestão, versionamento e release, ex: gi4low, semver

* [ ] Compreender como o software deve ser instalado e entregue em seus respectivos ambientes

* [ ] Conhecer o alcance da cobertura do software e quais testes devem ser aplicados na pipeline

* [ ] Avaliar processo de controle de qualidade do código

* [ ] Analisar processo de controle de segurança da aplicação

* [ ] Mapear se alguma etapa necessita de aprovação externa da gestão

# Estágios normalmente utilizados em um Pipeline:

* Tests
* Build
* Release
* Deploy
* Validate

# Os jobs que normalmente rodam dentro dos estágios:


* [ ] **Testes** estágios
* [ ] **Build** do software
* [ ] **Testes** do **binário gerado**
* [ ] **Build** da imagem **docker**
* [ ] **Teste** da imagem docker **(security e runtime)**
* [ ] **Push** da imagem para **repositório**
* [ ] **Sensibilização** da infraestrutura para **deploy**
* [ ] **Testes dinâmicos** com APP rodando
* [ ] **Testes** de **segurança** com APP rodando
* [ ] **Teste** de **carga** e estresse com APP rodando
* [ ] **Notificações** diversas

# O que precisa:

* [ ] **SCM** Ferramenta de gerenciamento de código
> (Git +GitLab)
* [ ] **CI/CD** Ferramenta pra construir a esteira 
> (GitLab CI + GitLab Runner)
* [ ] **Repositório** Ferramenta para armazenar os artefatos
> Nexus Repository
* [ ] **Infraestrutura** Para rodar as aplicações da Pipeline e as APPs
> Kubernetes Cluster + Rancher

# GitOps:
* [ ] **Conceito diferente de DevOps** Estudar o conceito

# DevOps:
* [ ] **Cultura** para equipes ageis

# Jenkins:
* [ ] **Servidor em java** para construção de jobs e pipelines

# Pipelines:
* [ ] **Esteira de Entrega** para entrega de software em produção


Enjoy!